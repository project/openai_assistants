# OpenAI Assistants
Provides AI (Artificial Intelligence) assistants that users can interact with and which base their replies on documents that have been added to them.

Currently limited to using the OpenAI API service, for which a paid subscription is required. Using alternative AI services is on the development roadmap, including self hosted AI.

For a full description of the module, visit the [project page](https://www.drupal.org/project/openai_assistants).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/openai_assistants).



## Requirements

This module requires the following modules:

- [OpenAI](https://www.drupal.org/project/openai)
- [AI Utilities](https://www.drupal.org/project/ai_utilities)
- [AJAX Extra](https://www.drupal.org/project/ajax_extra)
- [OpenAI Files](https://www.drupal.org/project/openai_files)

An [OpenAI API](https://openai.com/pricing) subscription is required.


## Installation

Install as you would normally install a contributed Drupal module. For further information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Enable the module: Administration > Extend.
2. Add an Assistant: Administration > Content > OpenAI Assistants
3. Add an Assistant: Administration > Structure > Block layout
4. Click "Place block"
5. Click "Add content block"
6. Choose "OpenAI Assistant"

## To DO

Substitute annotations - see "Message annotations":
https://platform.openai.com/docs/assistants/how-it-works/managing-threads-and-messages


## Maintainers

- Robert Castelo - [robert-castelo](https://www.drupal.org/u/robert-castelo)
