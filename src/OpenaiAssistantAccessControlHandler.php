<?php

namespace Drupal\openai_assistants;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for the openai assistant entity type.
 */
class OpenaiAssistantAccessControlHandler extends EntityAccessControlHandler {

  /**
   * Map of revision operations.
   *
   * Keys contain revision operations, where values are an array containing the
   * permission operation and entity operation.
   *
   * Permission operation is used to build the required permission, e.g.
   * 'permissionOperation all revisions', 'permissionOperation type revisions'.
   *
   * Entity operation is used to determine access, e.g for 'delete revision'
   * operation, an account must also have access to 'delete' operation on an
   * entity.
   */
  protected const REVISION_OPERATION_MAP = [
    'view all revisions' => ['view', 'view'],
    'view revision' => ['view', 'view'],
    'revert revision' => ['revert', 'update'],
    'delete revision' => ['delete', 'delete'],
  ];

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {

    [$revision_permission_operation, $entity_operation] = static::REVISION_OPERATION_MAP[$operation] ?? [
      NULL,
      NULL,
    ];

    // Revision operations.
    if ($revision_permission_operation) {
      // If user doesn't have any of these then quit.
      if (!$account->hasPermission("$revision_permission_operation all revisions") && !$account->hasPermission('administer openai assistant')) {
        return AccessResult::neutral()->cachePerPermissions();
      }

      // If the user has the view all revisions permission and this is the view
      // all revisions operation then we can allow access.
      if ($operation === 'view all revisions') {
        return AccessResult::allowed()->cachePerPermissions();
      }

      // If this is the default revision, return access denied for revert or
      // delete operations.
      if ($entity->isDefaultRevision() && ($operation === 'revert revision' || $operation === 'delete revision')) {
        return AccessResult::forbidden()->addCacheableDependency($entity);
      }
      elseif ($account->hasPermission('administer openai assistant')) {
        return AccessResult::allowed()->cachePerPermissions();
      }

      // First check the access to the default revision and finally, if the
      // node passed in is not the default revision then check access to
      // that, too.
      $storage = $this->entityTypeManager->getStorage($entity->getEntityTypeId());
      $access = $this->access($storage->load($entity->id()), $entity_operation, $account, TRUE);
      if (!$entity->isDefaultRevision()) {
        $access = $access->andIf($this->access($entity, $entity_operation, $account, TRUE));
      }
      return $access->cachePerPermissions()->addCacheableDependency($entity);
    }

    switch ($operation) {
      case 'view':
        return AccessResult::allowedIfHasPermission($account, 'view openai assistant');

      case 'update':
        return AccessResult::allowedIfHasPermissions(
            $account,
            ['edit openai assistant', 'administer openai assistant'],
            'OR',
        );

      case 'delete':
        return AccessResult::allowedIfHasPermissions(
            $account,
            ['delete openai assistant', 'administer openai assistant'],
            'OR',
        );

      default:
        // No opinion.
        return AccessResult::neutral();
    }

  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermissions(
          $account,
          ['create openai assistant', 'administer openai assistant'],
          'OR',
      );
  }

}
