<?php

namespace Drupal\openai_assistants\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an OpenAI Assistant block.
 *
 * @Block(
 *   id = "openai_assistant_block",
 *   admin_label = @Translation("OpenAI Assistant Block"),
 *   category = @Translation("OpenAI Assistant blocks"),
 *   deriver = "Drupal\openai_assistants\Plugin\Derivative\OpenaiAssistantBlockDeriver"
 * )
 */
class OpenaiAssistantBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new OpenaiAssistantBlock.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $download_prompt_default = t("Provide the previous responses formatted as a page of HTML which includes <!DOCTYPE html>. The reply should only include HTML. Where appropriate use headings, paragraphs, bold, and bullet points.");

    $openai_assistants = [
      'download' => FALSE,
      'download_prompt' => $download_prompt_default,
      'threads' => FALSE,
    ];

    return ['openai_assistants' => $openai_assistants];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {

    $conf = $this->configuration['openai_assistants'];

    $form['openai_assistants'] = [
      '#type' => 'fieldset',
      '#title' => t('Assistant Options'),
    ];

    $form['openai_assistants']['download'] = [
      '#type' => 'checkbox',
      '#title' => t('Download'),
      '#description' => t('Enable download button.'),
      '#default_value' => $conf['download'],
    ];

    $form['openai_assistants']['download_prompt'] = [
      '#type' => 'textarea',
      '#title' => t('Download Prompt'),
      '#description' => t('Prompt used to generate the contents of a downloadable file.'),
      '#default_value' => $conf['download_prompt'],
      '#states' => [
        'visible' => [
          ':input[name="settings[openai_assistants][download]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['openai_assistants']['threads'] = [
      '#type' => 'checkbox',
      '#title' => t('Muliple Chats'),
      '#description' => t('User can have multiple chats with an assistant and switch between them.'),
      '#default_value' => $conf['threads'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['openai_assistants'] = $form_state->getValue('openai_assistants');
    // $this->blockManager->clearCachedDefinitions();
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $entity_id = $this->getDerivativeId();
    $entity_storage = $this->entityTypeManager->getStorage('openai_assistant');
    $entity = $entity_storage->load($entity_id);
    $conf = $this->configuration['openai_assistants'];

    if ($entity) {

      $conf['assistant_id'] = $entity_id;
      $conf['title'] = $this->configuration['label'];
      $conf['block_id'] = $this->configuration['id'];
      $form = \Drupal::formBuilder()
        ->getForm('Drupal\openai_assistants\Form\OpenaiChatForm', $conf);

      return [
        '#theme' => 'block',
        '#attributes' => ['class' => ['openai-assistant-block']],
        '#configuration' => $this->configuration,
        '#plugin_id' => $this->pluginId,
        '#base_plugin_id' => $this->pluginDefinition,
        '#derivative_plugin_id' => $this->getDerivativeId(),
        'content' => $form,
      ];
    }

    return [];
  }

}
