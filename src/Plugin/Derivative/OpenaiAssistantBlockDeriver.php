<?php

namespace Drupal\openai_assistants\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Derives block plugin definitions for each OpenAI Assistant entity.
 */
class OpenaiAssistantBlockDeriver extends DeriverBase implements ContainerDeriverInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new OpenaiAssistantBlockDeriver.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $this->derivatives = [];

    $entity_storage = $this->entityTypeManager->getStorage('openai_assistant');
    $entities = $entity_storage->loadMultiple();

    foreach ($entities as $entity_id => $entity) {
      $this->derivatives[$entity_id] = $base_plugin_definition;
      $this->derivatives[$entity_id]['admin_label'] = $entity->label();
      $this->derivatives[$entity_id]['config_dependencies']['config'] = [$entity->getConfigDependencyName()];
    }

    return parent::getDerivativeDefinitions($base_plugin_definition);
  }

}
