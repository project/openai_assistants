<?php

namespace Drupal\openai_assistants;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining an openai assistant entity type.
 */
interface OpenaiAssistantInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

}
