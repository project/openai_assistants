<?php

namespace Drupal\openai_assistants\Controller;

use Drupal\ai_utilities\FormatInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\StreamedResponse;

/**
 * Stream reply from API to the browser.
 */
class StreamReply extends ControllerBase {

  /**
   * Tempstore service.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected $tempStore;

  /**
   * The format output service.
   *
   * @var \Drupal\ai_utilities\FormatInterface
   */
  protected $format;

  /**
   * Output of stream.
   *
   * @var string
   */
  private $output = '';

  /**
   * Buffer of stream from API.
   *
   * @var string
   */
  private $buffer = '';

  /**
   * Complete output of stream.
   *
   * @var string
   */
  private $completed = '';

  /**
   * Creates a StreamReply object.
   *
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $tempstore
   *   The Tempstore service.
   * @param \Drupal\ai_utilities\FormatInterface $format
   *   The format output service.
   */
  public function __construct(PrivateTempStoreFactory $tempstore, FormatInterface $format) {
    $this->tempStore = $tempstore;
    $this->format = $format;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('tempstore.private'),
      $container->get('ai_utilities.format')
    );
  }

  /**
   * Set up stream response instance.
   */
  public function stream() {
    // Create a StreamedResponse instance.
    $response = new StreamedResponse();

    // Set headers to inform the client to expect streamed content.
    $response->headers->set('Content-Type', 'text/event-stream');
    $response->headers->set('X-Accel-Buffering', 'no');
    $response->headers->set('Cache-Control', 'no-cache');

    // Set the callback function that will stream the content.
    $response->setCallback([$this, 'outputStream']);

    return $response;
  }

  /**
   * Callback to output streamed response to the browser.
   */
  public function outputStream() {
    // Ensure that the output buffering is turned off.
    if (ob_get_level()) {
      ob_end_clean();
    }

    $this->apiStream();
  }

  /**
   * Get stream response from API and display it in the browser.
   */
  public function apiStream() {
    // /** @var \OpenAI\Client $client */
    $client = \Drupal::service('openai.client');

    $settings = $this->tempStore->get('openai_assistants_block_assistant')->get('settings');
    $run_id = $settings['run_id'];

    // Create a stream if no stream exists.
    if (empty($run_id)) {
      $stream = $client->threads()->runs()->createStreamed(
        threadId: $settings['thread_id'],
        parameters: [
          'assistant_id' => $settings['assistant_id'],
        ],
      );
    }
    // Get current stream.
    else {
      $stream = $client->threads()->runs()->retrieve(
        threadId: $settings['thread_id'],
        runId: $settings['run_id'],
      );
    }

    foreach ($stream as $response) {
      $run = $response->response;

      if ($response->event == 'thread.message.delta') {
        $content = $run->delta->content[0]->text->value;
        $this->completed .= $content;
        $this->processMarkdown($content);
        $this->displayOutput();
      }

    }

    // Output anything remaining in buffer.
    $this->output = $this->buffer;
    $this->displayOutput();

    return NULL;
  }

  /**
   * Display output in browser.
   */
  public function displayOutput() {
    if ($this->output) {

      $html = $this->format->markdownToHTML($this->output);

      for ($i = 0; $i < strlen($html); $i++) {
        echo $html[$i];
        flush();
        usleep(10000);
      }

      $this->output = '';
    }
  }

  /**
   * Buffer markdown blocks into chunks.
   */
  public function processMarkdown($content) {

    // Delineator for chunks of Markdown.
    $delineator = "/\n\s*\n/";

    // Append the new content to the buffer.
    $this->buffer .= $content;

    $chunks = preg_split($delineator, $this->buffer, -1, PREG_SPLIT_DELIM_CAPTURE);

    if ($chunks === FALSE) {
      return;
    }

    $chunks_count = count($chunks);

    if ($chunks_count == 1) {
      return;
    }

    $chunk_number = 0;

    foreach ($chunks as $chunk) {
      $chunk_number++;

      // Check if the last chunk is a delineator, if not add it to the buffer.
      if ($chunk_number == $chunks_count) {

        if ($chunk === $delineator) {
          $this->output .= $chunk;
        }
        else {
          // Last chunk without delineator, add it to the buffer.
          $this->buffer = $chunk;
        }
      }
      else {
        // Output all chunks except the last one.
        $this->output .= $chunk;
      }
    }
  }

}
