<?php

namespace Drupal\openai_assistants\Controller;

use Drupal\Core\Entity\Controller\EntityViewController;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a controller to render a single openai_assistant.
 */
class OpenaiAssistantViewController extends EntityViewController {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The entity repository service.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * Creates a NodeViewController object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, RendererInterface $renderer, AccountInterface $current_user, EntityRepositoryInterface $entity_repository) {
    parent::__construct($entity_type_manager, $renderer);
    $this->currentUser = $current_user;
    $this->entityRepository = $entity_repository;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
          $container->get('entity_type.manager'),
          $container->get('renderer'),
          $container->get('current_user'),
          $container->get('entity.repository')
      );
  }

  /**
   * {@inheritdoc}
   */
  public function view(EntityInterface $openai_assistant, $view_mode = 'full', $langcode = NULL) {
    return parent::view($openai_assistant, $view_mode);
  }

  /**
   * The _title_callback for the page that renders a single openai_assistant.
   *
   * @param \Drupal\Core\Entity\EntityInterface $openai_assistant
   *   The current openai_assistant.
   *
   * @return string
   *   The page title.
   */
  public function title(EntityInterface $openai_assistant) {
    return $this->entityRepository->getTranslationFromContext($openai_assistant)->label();
  }

}
