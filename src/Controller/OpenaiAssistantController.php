<?php

namespace Drupal\openai_assistants\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\Sql\SqlEntityStorageInterface;
use Drupal\Core\Link;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Url;
use Drupal\openai_assistants\OpenaiAssistantInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns responses for OpenaiAssistant routes.
 */
class OpenaiAssistantController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The entity repository service.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * Constructs a OpenaiAssistantController object.
   *
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository.
   */
  public function __construct(DateFormatterInterface $date_formatter, RendererInterface $renderer, EntityRepositoryInterface $entity_repository) {
    $this->dateFormatter = $date_formatter;
    $this->renderer = $renderer;
    $this->entityRepository = $entity_repository;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('date.formatter'),
      $container->get('renderer'),
      $container->get('entity.repository')
    );
  }

  /**
   * Displays a openai_assistant revision.
   *
   * @param \Drupal\openai_assistants\OpenaiAssistantInterface $openai_assistant_revision
   *   The openai_assistant revision.
   *
   * @return array
   *   An array suitable for \Drupal\Core\Render\RendererInterface::render().
   */
  public function revisionShow(OpenaiAssistantInterface $openai_assistant_revision) {
    $node_view_controller = new OpenaiAssistantViewController($this->entityTypeManager(), $this->renderer, $this->currentUser(), $this->entityRepository);
    $page = $node_view_controller->view($openai_assistant_revision);
    unset($page['openai_assistants'][$openai_assistant_revision->id()]['#cache']);
    return $page;
  }

  /**
   * Page title callback for a openai_assistant revision.
   *
   * @param \Drupal\openai_assistants\OpenaiAssistantInterface $openai_assistant_revision
   *   The openai_assistant revision.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle(OpenaiAssistantInterface $openai_assistant_revision) {
    return $this->t(
      'Revision of %title from %date', [
        '%title' => $openai_assistant_revision->label(),
        '%date' => $this->dateFormatter->format($openai_assistant_revision->getRevisionCreationTime()),
      ]
    );
  }

  /**
   * Generates an overview table of older revisions of a openai_assistant.
   *
   * @param \Drupal\openai_assistants\OpenaiAssistantInterface $openai_assistant
   *   A openai_assistant object.
   *
   * @return array
   *   An array as expected by \Drupal\Core\Render\RendererInterface::render().
   */
  public function revisionOverview(OpenaiAssistantInterface $openai_assistant) {
    $langcode = $openai_assistant->language()->getId();
    $langname = $openai_assistant->language()->getName();
    $languages = $openai_assistant->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $storage = $this->entityTypeManager()->getStorage('openai_assistant');

    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', [
      '@langname' => $langname,
      '%title' => $openai_assistant->label(),
    ]) : $this->t('Revisions for %title', ['%title' => $openai_assistant->label()]);
    $header = [$this->t('Revision'), $this->t('Operations')];

    $rows = [];
    $default_revision = $openai_assistant->getRevisionId();
    $current_revision_displayed = FALSE;

    foreach ($this->getRevisionIds($openai_assistant, $storage) as $vid) {
      /**
       * @var \Drupal\openai_assistants\OpenaiAssistantInterface $revision
       */
      $revision = $storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)
        ->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = $this->dateFormatter->format($revision->revision_timestamp->value, 'short');

        // We treat also the latest translation-affecting revision as current
        // revision, if it was the default revision, as its values for the
        // current language will be the same of the current default revision in
        // this case.
        $is_current_revision = $vid == $default_revision || (!$current_revision_displayed && $revision->wasDefaultRevision());
        if (!$is_current_revision) {
          $link = Link::fromTextAndUrl($date, new Url('entity.openai_assistant.revision', [
            'openai_assistant' => $openai_assistant->id(),
            'openai_assistant_revision' => $vid,
          ]))->toString();
        }
        else {
          $link = $openai_assistant->toLink($date)->toString();
          $current_revision_displayed = TRUE;
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => $this->renderer->renderPlain($username),
              'message' => [
                '#markup' => $revision->revision_log->value,
                '#allowed_tags' => Xss::getHtmlTagList(),
              ],
            ],
          ],
        ];
        // @todo Simplify once https://www.drupal.org/node/2334319 lands.
        $this->renderer->addCacheableDependency($column['data'], $username);
        $row[] = $column;

        if ($is_current_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];

          $rows[] = [
            'data' => $row,
            'class' => ['revision-current'],
          ];
        }
        else {
          $links = [];
          if ($revision->access('revert revision')) {
            $links['revert'] = [
              'title' => $vid < $openai_assistant->getRevisionId() ? $this->t('Revert') : $this->t('Set as current revision'),
              'url' => Url::fromRoute('openai_assistant.revision_revert_confirm', [
                'openai_assistant' => $openai_assistant->id(),
                'openai_assistant_revision' => $vid,
              ]),
            ];
          }

          if ($revision->access('delete revision')) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('openai_assistant.revision_delete_confirm', [
                'openai_assistant' => $openai_assistant->id(),
                'openai_assistant_revision' => $vid,
              ]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];

          $rows[] = $row;
        }
      }
    }

    $build['openai_assistant_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
      '#attributes' => ['class' => 'openai-assistant-revision-table'],
    ];

    $build['pager'] = ['#type' => 'pager'];

    return $build;
  }

  /**
   * Gets a list of openai_assistant revision IDs for specific openai_assistant.
   *
   * @param \Drupal\openai_assistants\OpenaiAssistantInterface $openai_assistant
   *   The openai_assistant entity.
   * @param \Drupal\Core\Entity\Sql\SqlEntityStorageInterface $storage
   *   The openai_assistantopenai_assistant storage handler.
   *
   * @return int[]
   *   OpenaiAssistant revision IDs (in descending order).
   */
  protected function getRevisionIds(OpenaiAssistantInterface $openai_assistant, SqlEntityStorageInterface $storage) {
    $result = $storage->getQuery()
      ->accessCheck(TRUE)
      ->allRevisions()
      ->condition($openai_assistant->getEntityType()
        ->getKey('id'), $openai_assistant->id())
      ->sort($openai_assistant->getEntityType()->getKey('revision'), 'DESC')
      ->pager(50)
      ->execute();
    return array_keys($result);
  }

}
