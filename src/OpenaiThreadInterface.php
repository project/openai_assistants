<?php

namespace Drupal\openai_assistants;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining an openai threads entity type.
 */
interface OpenaiThreadInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

  /**
   * Run the prompt message.
   */
  public function messageCreate(string $message);

  /**
   * Run the prompt message.
   */
  public function runCreate();

}
