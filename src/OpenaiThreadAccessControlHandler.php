<?php

namespace Drupal\openai_assistants;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for the openai threads entity type.
 */
class OpenaiThreadAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {

    switch ($operation) {
      case 'view':
        return AccessResult::allowedIfHasPermission($account, 'view openai threads');

      case 'update':
        return AccessResult::allowedIfHasPermissions(
            $account,
            ['edit openai threads', 'administer openai threads'],
            'OR',
        );

      case 'delete':
        return AccessResult::allowedIfHasPermissions(
            $account,
            ['delete openai threads', 'administer openai threads'],
            'OR',
        );

      default:
        // No opinion.
        return AccessResult::neutral();
    }

  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermissions(
          $account,
          ['create openai threads', 'administer openai threads'],
          'OR',
      );
  }

}
