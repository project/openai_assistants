<?php

declare(strict_types=1);

namespace Drupal\openai_assistants\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CssCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Ajax\OpenOffCanvasDialogCommand;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\Core\Url;
use Drupal\openai_assistants\Entity\OpenaiThread;
use OpenAI\Client;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form to start a chat with an AI Assistant.
 */
final class OpenaiChatForm extends FormBase {

  /**
   * Route to this form.
   *
   * @var string
   */
  protected $formRoute = 'openai_assistants.chat_form';

  /**
   * Tempstore service.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected $tempStore;

  /**
   * Settings from block displaying form.
   *
   * @var mixed
   */
  protected $state;

  /**
   * OpenAI Client.
   *
   * @var \OpenAI\Client
   */
  protected $openaiClient;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a form object.
   *
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $tempstore
   *   The tempstore factory.
   * @param \OpenAI\Client $openai_client
   *   The OpenAI API Client.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(PrivateTempStoreFactory $tempstore, Client $openai_client, EntityTypeManagerInterface $entity_type_manager) {
    $this->tempStore = $tempstore;
    $this->openaiClient = $openai_client;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('tempstore.private'),
      $container->get('openai.client'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'openai_assistants_chat_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $state = []): array {
    // Set state including settings from block displaying this form.
    $this->stateSet($state);

    // Add all fields and buttons.
    $form = $this->all($form, $form_state);

    // Identify AJAX calls.
    $trigger = $form_state->getTriggeringElement();
    $callback = $trigger['#ajax']['callback'] ?? '';

    // Updates for AJAX callbacks to load into the form.
    switch ($callback) {
      case '::promptRequestAjax':
        $this->promptRequest($form_state->getValue('prompt'));
        break;

      default:
        break;
    }

    // Store state so it's available in AJAX callbacks and other forms.
    $this->stateStore();

    return $form;
  }

  /**
   * Set state so it can be accessed in AJAX callbacks.
   *
   * Include settings from block which are not available when form rebuilds.
   */
  private function stateSet($state) {
    $tempstore = $this->tempStore->get('openai_assistants_block_assistant');

    if (!empty($state)) {
      $tempstore->set('settings', $state);
    }

    $this->state = $tempstore->get('settings');
  }

  /**
   * Store state so it persists across AJAX calls and used in other forms.
   */
  private function stateStore() {
    $tempstore = $this->tempStore->get('openai_assistants_block_assistant');
    $tempstore->set('settings', $this->state);
  }

  /**
   * All the form fields and buttons.
   */
  public function all(array $form, FormStateInterface $form_state): array {

    $form['prompt'] = [
      '#type' => 'textfield',
      '#default_value' => '',
      '#placeholder' => $this->t('Ask me a question'),
      '#attributes' => [
        'class' => [
          'prompt-field',
        ],
      ],
    ];

    // Block title used as modal title.
    // Store in form so tempstore not needed in AJAX callback.
    $form['label'] = [
      '#type' => 'value',
      '#value' => $form_state->getValue('label', $this->state['title']),
    ];

    // Save and Cancel buttons.
    $form = $this->actions($form, $form_state);

    // Add the core AJAX library.
    $form['#attached']['library'][] = 'core/drupal.ajax';
    $form['#attached']['library'][] = 'openai_assistants/openai-assistants-ui';

    return $form;
  }

  /**
   * Call AI API to create a thread and set first prompt message.
   *
   * Creates a thread if one does not already exist.
   *
   * @param string $prompt
   *   (Optional) The prompt to run.
   */
  public function promptRequest($prompt = ''): void {
    $assistant_id = $this->state['assistant_id'] ?? '';
    $thread_id = $this->state['thread_id'] ?? '';
    $this->state['prompt'] = $prompt;

    // Create new thread or continue existing one.
    if (empty($thread_id)) {

      $thread_parameters = [
        'label' => $this->reduceString($prompt),
        'assistant_id' => $assistant_id,
      ];

      $thread = OpenaiThread::create($thread_parameters);
      $thread->save();
      $thread_id = $this->state['thread_id'] = $thread->id();
    }
    else {
      $this->openaiClient->threads()->messages()->create(
        $thread_id, [
          'role' => 'user',
          'content' => $prompt,
        ]
      );
    }

    /** @var \Drupal\openai_assistants\OpenaiThreadInterface $entity */
    $entity = $this->entityTypeManager->getStorage('openai_thread')->load($thread_id);
    $entity->messageCreate($prompt);
    $entity->save();
  }

  /**
   * Submit and other buttons to perform an action on the whole form.
   */
  public function actions(array $form, FormStateInterface $form_state) {
    $actions = [];
    $actions['#type'] = 'actions';

    $actions['save'] = [
      '#type' => 'button',
      '#button_type' => 'primary',
      '#value' => t('Send'),
      '#ajax' => [
        'callback' => '::promptRequestAjax',
        'url' => Url::fromRoute($this->formRoute),
        'event' => 'mousedown',
        'prevent' => 'click',
        'options' => [
          'query' => [
            FormBuilderInterface::AJAX_FORM_REQUEST => TRUE,
          ],
        ],
        'progress' => [
          'type' => 'none',
        ],
      ],
    ];

    // Add other query parameters as RenderElement::preRenderAjaxForm() does.
    $actions['save']['#ajax']['options']['query'] += \Drupal::request()->query->all();
    $form['actions'] = $actions;

    // Use the form route to process AJAX calls and submitions.
    $form['#action'] = Url::fromRoute($this->formRoute)->toString();

    return $form;
  }

  /**
   * Ajax callback after prompt has been sent to AI API.
   */
  public static function promptRequestAjax(array &$form, FormStateInterface $form_state): AjaxResponse {
    // AJAX callback must always return an AJAX response.
    $response = new AjaxResponse();

    // Clear the prompt input field.
    $response->addCommand(new InvokeCommand('#edit-prompt', 'val', ['']));

    // Hide prompt block.
    $response->addCommand(new CssCommand('.openai-assistant-block', ['display' => 'none']));

    // Display off canvas dialog.
    $title = $form_state->getValue('label');
    $content = \Drupal::formBuilder()
      ->getForm('Drupal\openai_assistants\Form\DialogForm', TRUE);

    $dialog_options = [
      'dialogClass' => 'openai-assistant-dialog-offcanvas',
      'width' => '40%',
    ];

    // Attach the libraries needed to use the OpenDialogCommand response.
    $attachments['library'][] = 'core/drupal.dialog.ajax';
    $attachments['library'][] = 'core/drupal.dialog.off_canvas';
    $response->setAttachments($attachments);

    // Add the open dialog command to the ajax response.
    $response->addCommand(new OpenOffCanvasDialogCommand($title, $content, $dialog_options));

    // Trigger hidden button to request fetching result of thread run.
    $selector = '.openai-assistants-reply-trigger';
    $method = 'mousedown';
    $response->addCommand(new InvokeCommand($selector, $method));

    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {}

  /**
   * Reduce the string length to 97 characters and append '...'.
   *
   * @param string $string
   *   The string being trimmed.
   * @param int $max_length
   *   The maximum length allowed before string is trimmed.
   * @param string $ellipsis
   *   Suffix to add to trimmed strings.
   *
   * @return string
   *   The trimmed string.
   */
  public function reduceString($string, $max_length = 100, $ellipsis = '...') {
    if (strlen($string) <= $max_length) {
      return $string;
    }
    else {
      return substr($string, 0, $max_length - strlen($ellipsis)) . $ellipsis;
    }
  }

}
