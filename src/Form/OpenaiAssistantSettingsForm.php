<?php

namespace Drupal\openai_assistants\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the openai assistant settings forms.
 */
class OpenaiAssistantSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['openai_assistants.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'openai_assistants_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('openai_assistants.settings');

    $form['streaming'] = [
      '#type' => 'radios',
      '#title' => $this->t('Reply Streaming'),
      '#default_value' => $config->get('streaming') ?: 'no_streaming',
      '#options' => [
        'no_streaming' => $this->t('No Streaming'),
        'polling' => $this->t('Polling'),
      ],
    ];

    $streaming_descriptions = [
      'no_streaming' => $this->t('Wait for complete reply before displaying it. Slow display but consumes least server resources.'),
      'polling' => $this->t('Browser requests as much of the reply as is available every 5 seconds. Fast display but multiple requests to server.'),
    ];

    foreach ($streaming_descriptions as $name => $description) {
      $form['streaming'][$name]['#description'] = $description;
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('openai_assistants.settings')
      ->set('streaming', $form_state->getValue('streaming'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
