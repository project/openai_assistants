<?php

namespace Drupal\openai_assistants\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the openai threads entity edit forms.
 */
class OpenaiThreadForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    /**
     * @var \OpenAI\Client $client
     */
    $client = \Drupal::service('openai.client');
    if ($this->entity->id()) {
      try {
        $run = $client->threads()
          ->runs()
          ->retrieve($this->entity->id(), $this->entity->get('current_run_id')->value);
      }
      catch (\Exception $e) {
      }

      $form['intro'] = [
        '#markup' => '<h2>Current run status: ' . (!empty($run) ? $run->status : $this->t('No current run')) . '</h2>',
        '#weight' => -5,
      ];
      $messages = $client->threads()->messages()->list($this->entity->id());
      $new_messages = [];

      foreach ($messages->data as $message) {
        $new_messages[] = $message->content[0]->text->value;
      }
      $this->entity->set('messages', array_reverse($new_messages));
    }

    $form = parent::form($form, $form_state);
    $form['assistant_id']['#disabled'] = TRUE;
    $form['current_run_id']['#disabled'] = TRUE;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->getEntity();

    if ($entity->get('assistant_id')->isEmpty()) {
      $entity->set('assistant_id', \Drupal::routeMatch()
        ->getParameter('openai_assistant'));
    }

    $result = parent::save($form, $form_state);

    $message_arguments = ['%label' => $entity->toLink()->toString()];
    $logger_arguments = [
      '%label' => $entity->label(),
      'link' => $entity->toLink($this->t('View'))->toString(),
    ];

    switch ($result) {
      case SAVED_NEW:
        $this->messenger()
          ->addStatus($this->t('New openai threads %label has been created.', $message_arguments));
        $this->logger('openai_assistants')
          ->notice('Created new openai threads %label', $logger_arguments);
        break;

      case SAVED_UPDATED:
        $this->messenger()
          ->addStatus($this->t('The openai threads %label has been updated.', $message_arguments));
        $this->logger('openai_assistants')
          ->notice('Updated openai threads %label.', $logger_arguments);
        break;
    }

    $form_state->setRedirect('entity.openai_thread.canonical', [
      'openai_thread' => $entity->id(),
      'openai_assistant' => $entity->get('assistant_id')->value,
    ]);

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $form_state->setRedirect('entity.openai_thread.edit_form', [
      'openai_assistant' => $this->entity->get('assistant_id')->value,
      'openai_thread' => $this->entity->id(),
    ]);
  }

}
