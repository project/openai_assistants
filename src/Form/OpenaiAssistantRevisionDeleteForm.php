<?php

namespace Drupal\openai_assistants\Form;

use Drupal\Core\Access\AccessManagerInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\openai_assistants\OpenaiAssistantInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for deleting a openai_assistant revision.
 *
 * @internal
 */
class OpenaiAssistantRevisionDeleteForm extends ConfirmFormBase {

  /**
   * The openai_assistant revision.
   *
   * @var \Drupal\openai_assistants\OpenaiAssistantInterface
   */
  protected $revision;

  /**
   * The openai_assistant storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $storage;

  /**
   * The access manager.
   *
   * @var \Drupal\Core\Access\AccessManagerInterface
   */
  protected $accessManager;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Constructs a new OpenaiAssistantRevisionDeleteForm.
   *
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The openai_assistant storage.
   * @param \Drupal\Core\Access\AccessManagerInterface $access_manager
   *   The access manager.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   */
  public function __construct(EntityStorageInterface $storage, AccessManagerInterface $access_manager, DateFormatterInterface $date_formatter) {
    $this->storage = $storage;
    $this->accessManager = $access_manager;
    $this->dateFormatter = $date_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $entity_type_manager = $container->get('entity_type.manager');
    return new static(
      $entity_type_manager->getStorage('openai_assistant'),
      $container->get('access_manager'),
      $container->get('date.formatter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'openai_assistant_revision_delete_confirm';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t(
      'Are you sure you want to delete the revision from %revision-date?', [
        '%revision-date' => $this->dateFormatter->format($this->revision->getRevisionCreationTime()),
      ]
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.openai_assistant.version_history', ['openai_assistantopenai_assistant' => $this->revision->id()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, OpenaiAssistantInterface $assistant_revision = NULL) {
    $this->revision = $assistant_revision;
    $form = parent::buildForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->storage->deleteRevision($this->revision->getRevisionId());

    $this->logger('content')
      ->info('@type: deleted %title revision %revision.', [
        '@type' => $this->revision->bundle(),
        '%title' => $this->revision->label(),
        '%revision' => $this->revision->getRevisionId(),
      ]);
    $this->messenger()
      ->addStatus(
        $this->t(
          'Revision from %revision-date of %title has been deleted.', [
            '%revision-date' => $this->dateFormatter->format($this->revision->getRevisionCreationTime()),
            '%title' => $this->revision->label(),
          ]
        )
      );
    // Set redirect to the revisions history page.
    $route_name = 'entity.openai_assistant.version_history';
    $parameters = ['openai_assistant' => $this->revision->id()];
    // If no revisions found, or the user does not have access to the revisions
    // page, then redirect to the canonical openai_assistant page instead.
    if (!$this->accessManager->checkNamedRoute($route_name, $parameters) || count($this->storage->revisionIds($this->revision)) === 1) {
      $route_name = 'entity.openai_assistant.canonical';
    }
    $form_state->setRedirect($route_name, $parameters);
  }

}
