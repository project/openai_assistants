<?php

namespace Drupal\openai_assistants\Form;

use Drupal\Core\Entity\ContentEntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Provides a form for deleting an openai_assistant entity.
 *
 * @ingroup content_entity_example
 */
class OpenaiAssistantDeleteForm extends ContentEntityConfirmFormBase {

  /**
   * Returns the question to ask the user.
   *
   * @return string
   *   The form question. The page title will be set to this value.
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete %name?', ['%name' => $this->entity->label()]);
  }

  /**
   * Returns the route to go to if the user cancels the action.
   *
   * @return \Drupal\Core\Url
   *   A URL object.
   */
  public function getCancelUrl() {
    return new Url('entity.openai_assistant.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   *
   * Delete the assistant and associated threads.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $entity = $this->getEntity();
    $entity->delete();
    $this->logger('openai_assistant')
      ->notice('deleted %title.', ['%title' => $this->entity->label()]);

    // Delete associated Threads.
    $this->deleteEntitiesWithReference($entity->id(), 'openai_thread', 'assistant_id');

    // Redirect to term list after delete.
    $form_state->setRedirect('entity.openai_assistant.collection');
  }

  /**
   * Deletes entities with reference.
   */
  public function deleteEntitiesWithReference($referencedEntityId, $entityType, $referenceField) {
    $storage = \Drupal::entityTypeManager()->getStorage($entityType);
    $entity_ids = $storage->getQuery()
      ->condition($referenceField, $referencedEntityId)
      ->accessCheck(FALSE)
      ->execute();

    if (!empty($entity_ids)) {
      $entities = $storage->loadMultiple($entity_ids);
      foreach ($entities as $entity) {
        $entity->delete();
        $this->logger('openai_thread')
          ->notice('deleted %title ID: %id.', [
            '%title' => $entity->label(),
            '%id' => $entity->id(),
          ]);
      }
    }
  }

}
