<?php

declare(strict_types=1);

namespace Drupal\openai_assistants\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use OpenAI\Client;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form to chat with an OpenAI assistant.
 */
final class AddExistingAssistantForm extends FormBase {

  /**
   * OpenAI client.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * OpenAI client.
   *
   * @var \OpenAI\Client
   */
  protected $client;

  /**
   * Creates the add form class.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, Client $openai_client) {
    $this->entityTypeManager = $entity_type_manager;
    $this->client = $openai_client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('openai.client'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'openai_assistants_add_existing_assistant';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $first_prompt = FALSE): array {

    try {
      // Get all assistants on OpenAI.
      $assistants_remote = $this->client->assistants()->list();
    }
    catch (\Exception $e) {
      $message = 'Failed to retrieve OpenAI assistants: @message.';
      \Drupal::logger('openai_assistants')->warning($message, ['@message' => $e->getMessage()]);
      \Drupal::messenger()->addWarning(t('Failed to retrieve OpenAI assistants.'));
    }

    if (empty($assistants_remote)) {
      $form['assistant'] = [
        '#markup' => $this->t('There are no Assistants available on OpenAI.'),
      ];

      return $form;
    }

    $assistants = [];

    foreach ($assistants_remote->data as $assistant_data) {
      $assistants[$assistant_data->id] = $assistant_data->name;
    }

    // Remove assistants already on the site from the selection options.
    $openai_assistant_storage = $this->entityTypeManager->getStorage('openai_assistant');
    $assistants_local = $openai_assistant_storage->getQuery()
      ->accessCheck(FALSE)
      ->execute();

    if (is_array($assistants_local)) {
      $assistants_local = array_flip($assistants_local);
      $assistants = array_diff_key($assistants, $assistants_local);
    }

    $form['assistant'] = [
      '#type' => 'select',
      '#title' => $this->t('OpenAI Assistant'),
      '#options' => $assistants,
    ];

    $form['save'] = [
      '#type' => 'submit',
      '#value' => t('Save'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $assistant_id = $form_state->getValue('assistant');
    $assistant = $this->client->assistants()->retrieve($assistant_id);
    $openai_assistant_storage = $this->entityTypeManager->getStorage('openai_assistant');
    $openai_assistant = $openai_assistant_storage->create(
      [
        'id' => $assistant_id,
        'name' => $assistant->name,
        'model' => $assistant->model,
        'instructions' => $assistant->instructions,
      ]
    );

    $openai_assistant->save();
    $form_state->setRedirect('entity.openai_assistant.collection');
  }

}
