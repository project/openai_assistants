<?php

declare(strict_types=1);

namespace Drupal\openai_assistants\Form;

use Drupal\ai_utilities\FormatInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\AppendCommand;
use Drupal\Core\Ajax\BeforeCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Ajax\RemoveCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Provides a form to chat with an OpenAI assistant.
 */
final class DialogForm extends FormBase {

  /**
   * Route to this form.
   *
   * @var string
   */
  protected $formRoute = 'openai_assistants.dialog_form';

  /**
   * Tempstore service.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected $tempStore;

  /**
   * The application state.
   *
   * @var array
   */
  protected $state;

  /**
   * The format output service.
   *
   * @var \Drupal\ai_utilities\FormatInterface
   */
  protected $format;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Builds the dialog form class.
   */
  public function __construct(FormatInterface $format, PrivateTempStoreFactory $tempstore, EntityTypeManagerInterface $entity_type_manager) {
    $this->format = $format;
    $this->tempStore = $tempstore;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
        $container->get('ai_utilities.format'),
        $container->get('tempstore.private'),
        $container->get('entity_type.manager')
      );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'openai_assistants_dialog_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $first_prompt = FALSE): array {

    // Set state including settings from block that triggered this form.
    $this->stateSet();

    // Add all fields and buttons.
    $form = $this->all($form, $form_state);

    // When this form is called by form in block.
    if ($first_prompt) {
      $this->prompt($form);
      $this->replyPlaceholder($form);
    }

    // Identify AJAX calls.
    $trigger = $form_state->getTriggeringElement();
    $callback = $trigger['#ajax']['callback'] ?? '';

    // Updates that the AJAX callback will load into the form.
    switch ($callback) {
      // Display the prompt and waiting for reply effect.
      case '::promptAddAjax':
        $this->state['prompt'] = $form_state->getValue('prompt');
        $this->prompt($form);
        $this->replyPlaceholder($form);
        $this->messageCreate($this->state['thread_id'], $this->state['prompt']);
        break;

      // Change which thread is displayed.
      case '::changeThreadCallback':
        $this->changeThread($form, $form_state);
        break;

      // Display reply.
      case '::noStreamingReplyAjax':
        $reply = $this->runCreate($this->state['thread_id']);
        $this->reply($form, $reply);
        break;

      case '::pollingReplyAjax':
        $reply = $this->pollingStream($this->state['assistant_id'], $this->state['thread_id']);
        $this->replyStreamed($form, $reply);
        break;

      default:
        if (isset($trigger['#type']) && $trigger['#type'] == 'button') {
          if (isset($trigger['#parents'][0]) && $trigger['#parents'][0] == 'download') {
            $this->download();
          }
        }
        break;
    }

    // Store state so it's available in AJAX callbacks and other forms.
    $this->stateStore();

    return $form;
  }

  /**
   * Set state so it can be accessed in AJAX callbacks.
   *
   * Include settings from block which are not available when form rebuilds.
   */
  private function stateSet() {
    $this->state = $this->tempStore->get('openai_assistants_block_assistant')->get('settings');
  }

  /**
   * Store state so it can persist across AJAX calls and used in other forms.
   */
  private function stateStore() {
    $tempstore = $this->tempStore->get('openai_assistants_block_assistant');
    $tempstore->set('settings', $this->state);
  }

  /**
   * All the form fields and buttons.
   */
  public function all(array $form, FormStateInterface $form_state): array {

    // Add required libraries.
    $form['#attached']['library'][] = 'core/drupal.ajax';
    $form['#attached']['library'][] = 'openai_assistants/openai-assistants-ui';
    $form['#attached']['library'][] = 'openai_assistants/openai-assistants.reply-await';
    $form['#attached']['library'][] = 'openai_assistants/openai-assistants.scroll';
    $form['#attached']['library'][] = 'openai_assistants/openai-assistants.dialog-close';

    $config = $this->config('openai_assistants.settings');

    if ($config->get('streaming') == 'no_streaming') {
      $form['#attached']['library'][] = 'openai_assistants/openai-assistants.typewriter';
      $form['#attached']['library'][] = 'openai_assistants/tameemsafi.typewriterjs';
    }
    else {
      $form['#attached']['library'][] = 'openai_assistants/stream_reply';
    }

    // Config option to show threads.
    if (isset($this->state['threads']) && $this->state['threads']) {
      $this->threads($form, $form_state);
    }

    $form['#prefix'] = '<div id="openai-assistant-dialog-offcanvas">';
    $form['#suffix'] = '</div>';

    $form['thread'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [
          'thread',
        ],
      ],
    ];

    $form['thread']['messages'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [
          'thread-messages',
        ],
      ],
    ];

    $form['thread']['form_controls'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [
          'form-controls',
        ],
      ],
    ];

    $form['thread']['form_controls']['prompt'] = [
      '#type' => 'textarea',
      '#default_value' => '',
      '#placeholder' => $this->t('Ask me a question'),
      '#rows' => 3,
      '#attributes' => [
        'id' => 'openai-assistants-prompt-field',
      ],
    ];

    // Buttons.
    $form = $this->actions($form, $form_state);

    return $form;
  }

  /**
   * Build the thread form.
   */
  public function threads(array &$form, FormStateInterface $form_state) {

    $current_user = \Drupal::currentUser();
    $storage = $this->entityTypeManager->getStorage('openai_thread');
    $openai_thread_ids = $storage->getQuery()
      ->condition('uid', $current_user->id())
      ->condition('assistant_id', $this->state['assistant_id'])
      ->sort('created', 'DESC')
      ->accessCheck(FALSE)
      ->execute();

    // Do not display threads switcher if only the initial thread exists.
    if (count($openai_thread_ids) < 2) {
      return;
    }

    $form['threads_list'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [
          'threads-list',
        ],
      ],
    ];

    $form['threads_list']['threads'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [
          'threads',
        ],
      ],
    ];

    /** @var \Drupal\Core\Config\Entity\ConfigEntityInterface; $thread */
    foreach ($storage->loadMultiple($openai_thread_ids) as $thread) {

      $thread_label = $thread->get('messages')->first()?->value;

      if (!empty($thread_label)) {
        $label = strlen($thread_label) > 70 ? substr($thread_label, 0, 70) . '...' : $thread_label;

        $form['threads_list']['threads'][$thread->id()] = [
          '#type' => 'button',
          '#value' => $label,
          '#ajax' => [
            'callback' => '::changeThreadCallback',
            'url' => Url::fromRoute($this->formRoute),
            'event' => 'mousedown',
            'prevent' => 'click',
            'progress' => ['type' => 'none'],
            'options' => [
              'query' => [
                FormBuilderInterface::AJAX_FORM_REQUEST => TRUE,
              ],
            ],
          ],
        ];
      }
    }

    $form['toggle'] = [
      '#type' => 'button',
      '#value' => $this->t('>'),
      '#attributes' => [
        'class' => [
          'threads-toggle',
        ],
      ],
      '#ajax' => [
        'callback' => '::toggleThreadsCallback',
        'url' => Url::fromRoute($this->formRoute),
        'event' => 'mousedown',
        'prevent' => 'click',
        'options' => [
          'query' => [
            FormBuilderInterface::AJAX_FORM_REQUEST => TRUE,
          ],
        ],
      ],
    ];

  }

  /**
   * Submit and other buttons to perform an action on the whole form.
   */
  public function actions(array $form, FormStateInterface $form_state) {

    $actions = [];
    $actions['#type'] = 'actions';

    // Send prompt button.
    $actions['save'] = [
      '#type' => 'button',
      '#button_type' => 'primary',
      '#value' => t('Send'),
      '#attributes' => [
        'class' => ['openai-assistants-prompt-send'],
      ],
      '#ajax' => [
        'callback' => '::promptAddAjax',
        'url' => Url::fromRoute($this->formRoute),
        'event' => 'mousedown',
        'prevent' => 'click',
        'options' => [
          'query' => [
            FormBuilderInterface::AJAX_FORM_REQUEST => TRUE,
          ],
        ],
        'progress' => [
          'type' => 'none',
        ],
      ],
    ];

    // Use block config settings to control display of Download button.
    if (isset($this->state['download']) && $this->state['download']) {
      $actions['download'] = [
        '#type' => 'button',
        '#button_type' => 'primary',
        '#value' => t('Download'),
        '#attributes' => [
          'class' => ['openai-assistants-download'],
        ],
      ];
    }

    // Hidden button to trigger JS events.
    $config = $this->config('openai_assistants.settings');

    $actions['trigger'] = [
      '#type' => 'button',
      '#button_type' => 'secondary',
      '#value' => t('Trigger'),
      '#attributes' => [
        'class' => ['openai-assistants-reply-trigger'],
      ],
    ];

    if ($config->get('streaming') == 'no_streaming') {
      $actions['trigger']['#ajax'] = [
        'callback' => '::noStreamingReplyAjax',
        'url' => Url::fromRoute($this->formRoute),
        'event' => 'mousedown',
        'prevent' => 'click',
        'options' => [
          'query' => [
            FormBuilderInterface::AJAX_FORM_REQUEST => TRUE,
          ],
        ],
        'progress' => [
          'type' => 'none',
        ],
      ];
    }

    // Add other query parameters as RenderElement::preRenderAjaxForm() does.
    $actions['save']['#ajax']['options']['query'] += \Drupal::request()->query->all();
    $form['thread']['form_controls']['actions'] = $actions;

    // Use the form route to process AJAX calls and submitions.
    $form['#action'] = Url::fromRoute($this->formRoute)->toString();

    return $form;
  }

  /**
   * Download the response.
   */
  public function download() {

    /** @var \OpenAI\Client $client */
    $client = \Drupal::service('openai.client');

    $assistant_id = $this->state['assistant_id'] ?? '';
    $thread_id = $this->state['thread_id'] ?? '';

    $client->threads()->messages()->create(
          $thread_id, [
            'role' => 'user',
            'content' => $this->state['download_prompt'],
          ]
      );

    // Existing thread, initiate a run for a new prompt.
    $run_response = $client->threads()->runs()->create($thread_id, ['assistant_id' => $assistant_id]);
    $download_run_id = $run_response->id;

    while ($run_response->status !== 'completed') {
      sleep(1);

      $run_response = $client->threads()->runs()->retrieve(
        threadId: $thread_id,
        runId: $download_run_id,
      );
    }

    $messages = $client->threads()->messages()->list($thread_id, ['limit' => 1]);
    $output = $messages->data[0]->content[0]->text->value;
    $annotations = $messages->data[0]->content[0]->text->annotations;

    $html_tag_position = strpos($output, '<!DOCTYPE html>');

    if ($html_tag_position !== FALSE) {
      $output = substr($output, $html_tag_position);
    }

    // Remove everything after the </html> tag.
    $htmlClosingTagPosition = strpos($output, '</html>');

    if ($htmlClosingTagPosition !== FALSE) {
      $output = substr($output, 0, $htmlClosingTagPosition + strlen('</html>'));
    }

    // Create a response object.
    $response = new Response();

    // Set the content type and headers.
    $response->headers->set('Content-Type', 'application/html');
    $response->headers->set('Content-Disposition', 'attachment; filename="generated_file.html"');

    // Set the content of the response.
    $response->setContent($output);

    // Send the response.
    $response->send();
  }

  /**
   * Display user provided prompt in the form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   */
  public function prompt(array &$form): void {
    $form['thread']['messages']['prompt_display'] = [
      '#prefix' => '<div class="message-role message-role-user openai-assistant-scroll">' . $this->t('You') . '</div><div class="openai-assistant-prompt prompt-typewriter-effect">',
      '#markup' => $this->state['prompt'],
      '#suffix' => '</div>',
    ];
  }

  /**
   * Add reply placeholder to the form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   */
  public function replyPlaceholder(array &$form): void {
    $form['thread']['messages']['reply'] = [
      '#prefix' => '<div class="message-role message-role-assistant role-reply-await">' . $this->t('Assistant') . '</div>',
      '#markup' => '<div class="openai-assistant-reply message-reply-await"><span class="waiting"></span> </div>',
    ];
  }

  /**
   * Reply markup.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param string $reply
   *   The reply generated by AI.
   */
  public function reply(array &$form, $reply): void {
    $form['thread']['messages']['reply']['received'] = [
      '#prefix' => '<div id="openai-assistant-reply-content" class="hidden">',
      '#markup' => $reply,
      '#suffix' => '</div><div id="openai-assistant-reply-display"></div>',
    ];
  }

  /**
   * Reply markup.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param string $reply
   *   The reply generated by AI.
   */
  public function replyStreamed(array &$form, $reply): void {
    $form['thread']['messages']['reply']['stream'] = [
      '#prefix' => '<span class="openai-assistant-scroll">',
      '#markup' => $reply,
      '#suffix' => '</span>',
    ];
  }

  /**
   * Ajax callback after prompt has been sent to AI API.
   */
  public static function promptAddAjax(array &$form, FormStateInterface $form_state) : AjaxResponse {
    // AJAX callback must always return an AJAX response.
    $response = new AjaxResponse();

    // Clear and disable the prompt input field.
    $selector = '#openai-assistants-prompt-field';
    $response->addCommand(new InvokeCommand($selector, 'val', ['']));
    $response->addCommand(new InvokeCommand($selector, 'attr', ['disabled', TRUE]));

    // Disable the prompt send button.
    $selector = '.openai-assistant-dialog-offcanvas .thread .button--primary';
    $response->addCommand(new InvokeCommand($selector, 'attr', ['disabled', TRUE]));

    // Disable the Dowload button.
    if (isset($form['thread']['form_controls']['actions']['download'])) {
      $selector = '.openai-assistant-dialog-offcanvas .thread .openai-assistants-download';
      $response->addCommand(new InvokeCommand($selector, 'attr', ['disabled', TRUE]));
    }

    $selector = '.openai-assistant-dialog-offcanvas .thread .thread-messages';

    // Display the prompt.
    $response->addCommand(new AppendCommand($selector, $form['thread']['messages']['prompt_display']));

    // Display the reply placeholder.
    $response->addCommand(new AppendCommand($selector, $form['thread']['messages']['reply']));

    // Trigger hidden button to request fetching result of thread run.
    $selector = '.openai-assistants-reply-trigger';
    $method = 'mousedown';
    $response->addCommand(new InvokeCommand($selector, $method));

    return $response;
  }

  /**
   * Ajax callback after trigger button is invoked.
   */
  public static function noStreamingReplyAjax(array &$form, FormStateInterface $form_state) : AjaxResponse {
    // AJAX callback must always return an AJAX response.
    $response = new AjaxResponse();

    // Display the reply.
    $selector = '.openai-assistant-dialog-offcanvas .thread .thread-messages .message-reply-await .waiting';
    $response->addCommand(new BeforeCommand($selector, $form['thread']['messages']['reply']['received']));

    // Remove the waiting effect.
    $response->addCommand(new RemoveCommand($selector));

    // Set message as completed.
    $selector = '.openai-assistant-dialog-offcanvas .thread .thread-messages .message-reply-await';
    $response->addCommand(new InvokeCommand($selector, 'removeClass', ['message-reply-await']));

    // Enable the prompt form field.
    $selector = '#openai-assistants-prompt-field';
    $response->addCommand(new InvokeCommand($selector, 'removeAttr', ['disabled']));

    // Enable the prompt send button.
    $selector = '.openai-assistant-dialog-offcanvas .thread .button--primary';
    $response->addCommand(new InvokeCommand($selector, 'removeAttr', ['disabled']));
    $response->addCommand(new InvokeCommand($selector, 'removeClass', ['is-disabled']));

    // Enable the Download button.
    if (isset($form['thread']['form_controls']['actions']['download'])) {
      $selector = '.openai-assistant-dialog-offcanvas .thread .openai-assistants-download';
      $response->addCommand(new InvokeCommand($selector, 'removeAttr', ['disabled']));
    }

    return $response;
  }

  /**
   * Ajax callback after trigger button is invoked.
   */
  public static function pollingReplyAjax(array &$form, FormStateInterface $form_state) : AjaxResponse {
    // AJAX callback must always return an AJAX response.
    $response = new AjaxResponse();

    // Display the reply.
    $selector = '.openai-assistant-dialog-offcanvas .thread .thread-messages .message-reply-await';
    $response->addCommand(new AppendCommand($selector . ' .reply-received', $form['thread']['messages']['reply']['stream']));

    // @todo check if reply has completed.
    $response->addCommand(new RemoveCommand($selector . ' .waiting'));
    $response->addCommand(new InvokeCommand($selector . ' .reply-received', 'removeClass', ['reply-received']));
    $response->addCommand(new InvokeCommand($selector, 'removeClass', ['message-reply-await']));

    // Enable the prompt form field.
    $selector = '#openai-assistants-prompt-field';
    $response->addCommand(new InvokeCommand($selector, 'removeAttr', ['disabled']));

    // Enable the prompt send button.
    $selector = '.openai-assistant-dialog-offcanvas .thread .button--primary';
    $response->addCommand(new InvokeCommand($selector, 'removeAttr', ['disabled']));
    $response->addCommand(new InvokeCommand($selector, 'removeClass', ['is-disabled']));

    // Enable the Download button.
    if (isset($form['thread']['form_controls']['actions']['download'])) {
      $selector = '.openai-assistant-dialog-offcanvas .thread .openai-assistants-download';
      $response->addCommand(new InvokeCommand($selector, 'removeAttr', ['disabled']));
    }

    return $response;
  }

  /**
   * Change the thread.
   */
  public function changeThread(array &$form, FormStateInterface $form_state) {
    // Detect thread ID.
    $trigger = $form_state->getTriggeringElement();
    $thread_id = $trigger['#parents'][0];
    $this->state['thread_id'] = $thread_id;

    /** @var \OpenAI\Client $client */
    $client = \Drupal::service('openai.client');
    $messages_data = $client->threads()->messages()->list($thread_id);
    $messages = array_reverse($messages_data->data);

    foreach ($messages as $message) {

      $message_content = $message->content[0]->text->value;

      if ($message->role == 'assistant') {
        // Check if reply is HTML formatted.
        if ($this->format->isHtml($message_content)) {
          // Trim parts of message outside of HTML.
          $message = $this->format->trim($message_content);
        }
        else {
          // Fallback to Markdown formatting.
          $message_content = $this->format->trim($message_content, "```markdown");
          $message_content = $this->format->markdownToHtml($message_content);
        }

        $form['thread']['messages'][]['reply'] = [
          '#prefix' => '<div class="message-role message-role-assistant">' . $this->t('Assistant') . '</div><div class="openai-assistant-reply">',
          '#markup' => $message_content,
          '#suffix' => '</div>',
        ];
      }
      else {
        $form['thread']['messages'][]['prompt_display'] = [
          '#prefix' => '<div class="message-role message-role-user">' . $this->t('You') . '</div><div class="openai-assistant-prompt">',
          '#markup' => $message_content,
          '#suffix' => '</div>',
        ];
      }
    }
  }

  /**
   * AJAX callback to toggle the threads listing between hide/display.
   */
  public function toggleThreadsCallback(array &$form, FormStateInterface $form_state) : AjaxResponse {
    $response = new AjaxResponse();

    // Toggle the threads listing between hide/display.
    $selector = '.openai-assistant-dialog-offcanvas .threads-list';
    $method = 'toggle';
    $arguments = [500, 'swing'];
    $response->addCommand(new InvokeCommand($selector, $method, $arguments));

    // Toggle the thread between hide/display.
    $selector = '.openai-assistant-dialog-offcanvas .thread';
    $method = 'toggle';
    $arguments = [600, 'linear'];
    $response->addCommand(new InvokeCommand($selector, $method, $arguments));

    return $response;
  }

  /**
   * AJAX callback to change the thread being displayed.
   */
  public function changeThreadCallback(array &$form, FormStateInterface $form_state) : AjaxResponse {
    $response = new AjaxResponse();

    // Replace existing thread messages with new ones.
    $selector = '.openai-assistant-dialog-offcanvas .thread .thread-messages';
    $response->addCommand(new ReplaceCommand($selector, $form['thread']['messages']));

    // Toggle the threads listing to hide.
    $selector = '.openai-assistant-dialog-offcanvas .threads-list .threads';
    $method = 'hide';
    $arguments = [500, 'swing'];
    $response->addCommand(new InvokeCommand($selector, $method, $arguments));

    // Toggle the thread to display.
    $selector = '.openai-assistant-dialog-offcanvas .thread';
    $method = 'show';
    $arguments = [100, 'linear'];
    $response->addCommand(new InvokeCommand($selector, $method, $arguments));

    return $response;
  }

  /**
   * Add a message to a thread.
   *
   * @param string $thread_id
   *   ID of Thread.
   * @param string $prompt
   *   Prompt input by user.
   */
  public function messageCreate($thread_id, $prompt): void {
    /** @var \Drupal\openai_assistants\OpenaiThreadInterface $entity */
    $entity = $this->entityTypeManager->getStorage('openai_thread')->load($thread_id);
    $entity->messageCreate($prompt);
    $entity->save();
  }

  /**
   * Run a prompt and return a reply.
   *
   * @param string $thread_id
   *   ID of Thread.
   *
   * @return string
   *   Reply message generated by AI.
   */
  public function runCreate($thread_id): string {

    // /** @var \OpenAI\Client $client */
    $client = \Drupal::service('openai.client');

    /** @var \Drupal\openai_assistants\OpenaiThreadInterface $entity */
    $entity = $this->entityTypeManager->getStorage('openai_thread')->load($thread_id);
    $entity->runCreate();
    $entity->save();

    $run_id = $entity->get('current_run_id')->value;

    $status = '';

    while ($status !== 'completed') {
      sleep(1);

      $run_response = $client->threads()->runs()->retrieve(
        threadId: $thread_id,
        runId: $run_id,
      );

      $status = $run_response->status;
    }

    $messages = $client->threads()->messages()->list($thread_id, ['limit' => 1]);
    $message = $messages->data[0]->content[0]->text->value;

    // Check if reply is HTML formatted.
    if ($this->format->isHTML($message)) {
      // Trim parts of message outside of HTML.
      $message = $this->format->trim($message);
    }
    else {
      // Fallback to Markdown formatting.
      $message = $this->format->trim($message, "```markdown");
      $message = $this->format->markdownToHTML($message);
    }

    return $message;
  }

  /**
   * Streamed reply using polling.
   *
   * @param string $assistant_id
   *   Assistant ID.
   * @param string $thread_id
   *   Thread ID.
   *
   * @return string
   *   Streamed reply.
   */
  public function pollingStream($assistant_id, $thread_id) {
    // /** @var \OpenAI\Client $client */
    $client = \Drupal::service('openai.client');
    $run_id = $this->state['run_id'];
    $reply = '';

    // Create a stream if no stream exists.
    if (empty($run_id)) {
      $stream = $client->threads()->runs()->createStreamed(
        threadId: $thread_id,
        parameters: [
          'assistant_id' => $assistant_id,
        ],
      );
    }
    // Get current stream.
    else {
      $stream = $client->threads()->runs()->retrieve(
        threadId: $thread_id,
        runId: $run_id,
      );
    }

    foreach ($stream as $response) {
      $run = $response->response;

      if (empty($run_id)) {
        $run_id = $this->state['run_id'] = $run->id;
      }

      if ($response->event == 'thread.message.delta') {
        $token = $run->delta->content[0]->text->value;
        $reply .= $token;
      }

    }

    return $reply;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {}

}
