<?php

namespace Drupal\openai_assistants\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the openai assistant entity edit forms.
 */
class OpenaiAssistantForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  protected function getNewRevisionDefault() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $result = parent::save($form, $form_state);

    /**
     * @var \Drupal\openai_assistants\OpenaiAssistantInterface $entity
     */
    $entity = $this->getEntity();
    $entity->setNewRevision();

    $message_arguments = ['%label' => $entity->toLink()->toString()];
    $logger_arguments = [
      '%label' => $entity->label(),
      'link' => $entity->toLink($this->t('View'))->toString(),
    ];

    switch ($result) {
      case SAVED_NEW:
        $this->messenger()
          ->addStatus($this->t('New openai assistant %label has been created.', $message_arguments));
        $this->logger('openai_assistants')
          ->notice('Created new openai assistant %label', $logger_arguments);
        break;

      case SAVED_UPDATED:
        $this->messenger()
          ->addStatus($this->t('The openai assistant %label has been updated.', $message_arguments));
        $this->logger('openai_assistants')
          ->notice('Updated openai assistant %label.', $logger_arguments);
        break;
    }

    $form_state->setRedirect('entity.openai_assistant.canonical', ['openai_assistant' => $entity->id()]);

    return $result;
  }

}
