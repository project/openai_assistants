<?php

namespace Drupal\openai_assistants\Entity;

use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\RevisionableContentEntityBase;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\openai_assistants\OpenaiAssistantInterface;
use Drupal\user\EntityOwnerTrait;

/**
 * Defines the openai assistant entity class.
 *
 * @ContentEntityType(
 *   id = "openai_assistant",
 *   label = @Translation("OpenAI Assistant"),
 *   label_collection = @Translation("OpenAI Assistants"),
 *   label_singular = @Translation("openai assistant"),
 *   label_plural = @Translation("openai assistants"),
 *   label_count = @PluralTranslation(
 *     singular = "@count openai assistants",
 *     plural = "@count openai assistants",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\openai_assistants\OpenaiAssistantListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\openai_assistants\OpenaiAssistantAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\openai_assistants\Form\OpenaiAssistantForm",
 *       "edit" = "Drupal\openai_assistants\Form\OpenaiAssistantForm",
 *       "delete" = "Drupal\openai_assistants\Form\OpenaiAssistantDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "openai_assistant",
 *   revision_table = "openai_assistant_revision",
 *   show_revision_ui = TRUE,
 *   admin_permission = "administer openai assistant",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "revision_id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "owner" = "uid",
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_uid",
 *     "revision_created" = "revision_timestamp",
 *     "revision_log_message" = "revision_log",
 *   },
 *   links = {
 *     "collection" = "/admin/content/openai-assistant",
 *     "add-form" = "/admin/content/openai-assistant/add",
 *     "canonical" = "/admin/content/openai-assistant/{openai_assistant}",
 *     "edit-form" = "/admin/content/openai-assistant/{openai_assistant}/edit",
 *     "delete-form" = "/admin/content/openai-assistant/{openai_assistant}/delete",
 *     "threads" = "/admin/content/openai-assistant/{openai_assistant}/threads",
 *   },
 *   field_ui_base_route = "entity.openai_assistant.settings",
 * )
 */
class OpenaiAssistant extends RevisionableContentEntityBase implements OpenaiAssistantInterface {

  use EntityChangedTrait;
  use EntityOwnerTrait;

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);
    if (!$this->getOwnerId()) {
      // If no owner has been set explicitly, make the anonymous user the owner.
      $this->setOwnerId(0);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    if ($entity_type->hasKey('id')) {
      $fields[$entity_type->getKey('id')] = BaseFieldDefinition::create('string')
        ->setLabel(new TranslatableMarkup('ID'));
    }

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setRevisionable(TRUE)
      ->setLabel(new TranslatableMarkup('Name'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions(
              'form', [
                'type' => 'string_textfield',
                'weight' => 0,
              ]
          )
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions(
              'view', [
                'label' => 'hidden',
                'type' => 'string',
                'weight' => -5,
              ]
          )
      ->setDisplayConfigurable('view', TRUE);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setRevisionable(TRUE)
      ->setLabel(new TranslatableMarkup('Author'))
      ->setSetting('target_type', 'user')
      ->setDefaultValueCallback(static::class . '::getDefaultEntityOwner')
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions(
              'view', [
                'label' => 'above',
                'type' => 'author',
                'weight' => 15,
              ]
          )
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(new TranslatableMarkup('Authored on'))
      ->setDescription(new TranslatableMarkup('The time that the openai assistant was created.'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions(
              'form', [
                'type' => 'datetime_timestamp',
                'weight' => 20,
              ]
          )
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(new TranslatableMarkup('Changed'))
      ->setDescription(new TranslatableMarkup('The time that the openai assistant was last edited.'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $models = [];

    try {
      $models = \Drupal::service('openai.api')->getModels();
    }
    catch (\Exception $e) {
      $message = 'Failed to retrieve OpenAI models: @message. Check OpenAI API key has been set up';
      \Drupal::logger('openai_assistants')->warning($message, ['@message' => $e->getMessage()]);
    }

    $fields['model'] = BaseFieldDefinition::create('list_string')
      ->setLabel(new TranslatableMarkup('Model'))
      ->setRequired(TRUE)
      ->setSetting('allowed_values', $models)
      ->setDefaultValue('gpt-4-1106-preview')
      ->setRevisionable(TRUE)
      ->setDisplayOptions(
              'form', [
                'label' => 'inline',
                'type' => 'options_select',
                'weight' => 2,
              ]
          )
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['instructions'] = BaseFieldDefinition::create('string_long')
      ->setLabel(new TranslatableMarkup('Instructions'))
      ->setRevisionable(TRUE)
      ->setDefaultValue('')
      ->setDisplayOptions(
              'form', [
                'label' => 'above',
                'type' => 'string_textfield',
                'weight' => 3,
              ]
          )
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['file_ids'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Files'))
      ->setDescription(t('Add documents for the AI Assistant to base answers on.'))
      ->setSetting('target_type', 'media')
      ->setCardinality(20)
      ->setSetting('handler', 'default:media')
      ->setSetting(
              'handler_settings', [
                'target_bundles' => ['ai_data'],
                'auto_create' => TRUE,
                'auto_create_bundle' => '',

              ]
          )
      ->setDisplayOptions(
              'form', [
                'type' => 'media_library_widget',
                'weight' => 0,
                'settings' => [
                  'media_types' => ['ai_data'],
                ],
              ]
          )
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['threads'] = BaseFieldDefinition::create('entity_reference')
      ->setRevisionable(TRUE)
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setLabel(new TranslatableMarkup('Threads'))
      ->setSetting('target_type', 'openai_thread')
      ->setDisplayOptions(
              'form', [
                'type' => 'entity_reference_autocomplete',
                'settings' => [
                  'match_operator' => 'CONTAINS',
                  'size' => 60,
                  'placeholder' => '',
                ],
                'weight' => 15,
              ]
          )
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function save() {
    /**
* @var \OpenAI\Client $client
*/
    $client = \Drupal::service('openai.client');

    // Tools needs to be set to 'retrieval' or 'code_interpreter' for files to
    // be used by assistants. Hard coding to 'retrieval' for now.
    $tool_selected = ['type' => 'retrieval'];
    $tools = [(object) $tool_selected];

    $assistant_files = [];
    $media_entities = $this->get('file_ids')->referencedEntities();

    foreach ($media_entities as $media) {
      $remote_id = $media->get('field_remote_id')->value;

      // Check if file is already on remote and has an ID.
      if (empty($remote_id)) {
        $file = $media->get('field_media_file')->entity;

        $response = $client->files()->upload(
              [
                'purpose' => 'assistants',
                'file' => fopen($file->getFileUri(), 'r'),
              ]
          );

        $remote_id = $response->id;
        $media->set('field_remote_id', $response->id);
        $media->save();
      }

      $assistant_files[] = $remote_id;
    }

    sort($assistant_files);

    /**
* @var \Drupal\Core\Utility\Token $token
*/
    $token = \Drupal::service('token');

    // If there's no ID, it doesn't exist on OpenAI so create an assistant and
    // save the ID before saving.
    if (empty($this->id())) {
      $instructions = $token->replace(
            $this->get('instructions')->value,
            ['openai_assistant' => $this],
        );

      $response = $client->assistants()->create(
            [
              'instructions' => $instructions,
              'name' => $this->label(),
              'tools' => $tools,
              'model' => $this->get('model')->value,
              'file_ids' => $assistant_files,
            ]
        );

      $this->set('id', $response->id);
    }
    // Otherwise, retrieve the remote Assistant, check for new values and
    // update if required.
    else {
      $remote_assistant = $client->assistants()->retrieve($this->id());
      $fields = [
        'name',
        'model',
        'instructions',
      ];

      $updated_values = [];

      foreach ($fields as $field) {
        $new_value = $this->get($field)->value;

        if ($field === 'instructions') {
          $new_value = $token->replace(
                $new_value,
                ['openai_assistant' => $this],
            );
        }

        if ($remote_assistant->{$field} !== $new_value) {
          $updated_values[$field] = $new_value;
        }
      }

      if ($remote_assistant->fileIds !== $assistant_files) {
        $updated_values['file_ids'] = $assistant_files;
      }

      if (!empty($updated_values)) {
        $client->assistants()->modify($remote_assistant->id, $updated_values);
      }
    }

    parent::save();
  }

}
