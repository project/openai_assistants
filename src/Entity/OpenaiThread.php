<?php

namespace Drupal\openai_assistants\Entity;

use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\RevisionableContentEntityBase;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\openai_assistants\OpenaiThreadInterface;
use Drupal\user\EntityOwnerTrait;

/**
 * Defines the openai threads entity class.
 *
 * @ContentEntityType(
 *   id = "openai_thread",
 *   label = @Translation("OpenAI Thread"),
 *   label_collection = @Translation("OpenAI Threads"),
 *   label_singular = @Translation("openai thread"),
 *   label_plural = @Translation("openai threads"),
 *   label_count = @PluralTranslation(
 *     singular = "@count openai thread",
 *     plural = "@count openai threads",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\openai_assistants\OpenaiThreadListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\openai_assistants\OpenaiThreadAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\openai_assistants\Form\OpenaiThreadForm",
 *       "edit" = "Drupal\openai_assistants\Form\OpenaiThreadForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" =
 *   "Drupal\openai_assistants\Entity\Routing\ThreadHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "openai_thread",
 *   revision_table = "openai_thread_revision",
 *   show_revision_ui = TRUE,
 *   admin_permission = "administer openai thread",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "revision_id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "owner" = "uid",
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_uid",
 *     "revision_created" = "revision_timestamp",
 *     "revision_log_message" = "revision_log",
 *   },
 *   links = {
 *     "collection" = "/admin/content/openai-thread",
 *     "assistant-collection" =
 *   "/admin/content/openai-assistant/{openai_assistant}/threads",
 *     "add-form" =
 *   "/admin/content/openai-assistant/{openai_assistant}/threads/add",
 *     "canonical" =
 *   "/admin/content/openai-assistant/{openai_assistant}/threads/{openai_thread}",
 *     "edit-form" =
 *   "/admin/content/openai-assistant/{openai_assistant}/threads/{openai_thread}/edit",
 *     "delete-form" =
 *   "/admin/content/openai-assistant/{openai_assistant}/threads/{openai_thread}/delete",
 *   },
 *   field_ui_base_route = "entity.openai_thread.settings",
 * )
 */
class OpenaiThread extends RevisionableContentEntityBase implements OpenaiThreadInterface {

  use EntityChangedTrait;
  use EntityOwnerTrait;

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel) {
    $uri_route_parameters = parent::urlRouteParameters($rel);
    $uri_route_parameters['openai_assistant'] = $this->getAssistantId();
    return $uri_route_parameters;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);
    if (!$this->getOwnerId()) {
      // If no owner has been set explicitly, make the anonymous user the owner.
      $this->setOwnerId(0);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields = parent::baseFieldDefinitions($entity_type);

    if ($entity_type->hasKey('id')) {
      $fields[$entity_type->getKey('id')] = BaseFieldDefinition::create('string')
        ->setLabel(new TranslatableMarkup('ID'));
    }

    $fields['label'] = BaseFieldDefinition::create('string')
      ->setRevisionable(TRUE)
      ->setLabel(new TranslatableMarkup('Label'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions(
              'form', [
                'type' => 'string_textfield',
                'weight' => -5,
              ]
          )
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions(
              'view', [
                'label' => 'hidden',
                'type' => 'string',
                'weight' => -5,
              ]
          )
      ->setDisplayConfigurable('view', TRUE);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setRevisionable(TRUE)
      ->setLabel(new TranslatableMarkup('Author'))
      ->setSetting('target_type', 'user')
      ->setDefaultValueCallback(static::class . '::getDefaultEntityOwner')
      ->setDisplayOptions(
              'form', [
                'type' => 'entity_reference_autocomplete',
                'settings' => [
                  'match_operator' => 'CONTAINS',
                  'size' => 60,
                  'placeholder' => '',
                ],
                'weight' => 15,
              ]
          )
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions(
              'view', [
                'label' => 'above',
                'type' => 'author',
                'weight' => 15,
              ]
          )
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(new TranslatableMarkup('Authored on'))
      ->setDescription(new TranslatableMarkup('The time that the OpenAI thread was created.'))
      ->setDisplayOptions(
              'view', [
                'label' => 'above',
                'type' => 'timestamp',
                'weight' => 20,
              ]
          )
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions(
              'form', [
                'type' => 'datetime_timestamp',
                'weight' => 20,
              ]
          )
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(new TranslatableMarkup('Changed'))
      ->setDescription(new TranslatableMarkup('The time that the OpenAI thread was last edited.'));

    $fields['assistant_id'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Assistant ID'))
      ->setDescription(new TranslatableMarkup('The Assistant ID this Thread belongs to.'))
      ->setSetting('max_length', 255)
      ->setRevisionable(TRUE)
      ->setDisplayOptions(
              'form', [
                'label' => 'inline',
                'type' => 'string_textfield',
                'weight' => 1,
              ]
          )
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['messages'] = BaseFieldDefinition::create('string_long')
      ->setLabel(new TranslatableMarkup('Messages'))
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setRevisionable(TRUE)
      ->setDisplayOptions(
          'form', [
            'label' => 'above',
            'type' => 'string_textfield',
            'weight' => 3,
          ]
        )
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['current_run_id'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Current Run ID'))
      ->setDescription(new TranslatableMarkup('The Run ID for the latest run.'))
      ->setSetting('max_length', 255)
      ->setRevisionable(TRUE)
      ->setDisplayOptions(
              'form', [
                'label' => 'inline',
                'type' => 'string_textfield',
                'weight' => 1,
              ]
          )
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

  /**
   * Get the Assistant ID.
   *
   * @return string
   *   The Assistant ID.
   */
  public function getAssistantId(): string {
    return $this->get('assistant_id')->value;
  }

  /**
   * Gets the created timestamp.
   *
   * @return int|null
   *   The created timestamp.
   */
  public function getCreatedTime(): ?int {
    $value = $this->get('created')->value;
    return isset($value) ? (int) $value : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function save() {
    if ($this->isNew()) {
      /**
       * @var \OpenAI\Client $client
       */
      $client = \Drupal::service('openai.client');

      $messages = array_map(
        fn (array $item) => $item['value'],
        $this->get('messages')->getValue(),
      );

      $response = $client->threads()->create(['messages' => $messages]);

      $this->set('id', $response->id);
    }

    return parent::save();
  }

  /**
   * Add a prompt message to the thread.
   */
  public function messageCreate(string $message) {
    /** @var \OpenAI\Client $client */
    $client = \Drupal::service('openai.client');

    // Add the message to the thread.
    $client->threads()->messages()->create(
      $this->id(), [
        'role' => 'user',
        'content' => $message,
      ]
    );

    $this->messages[] = $message;
  }

  /**
   * Run the prompt message.
   */
  public function runCreate(): static {
    /** @var \OpenAI\Client $client */
    $client = \Drupal::service('openai.client');

    // Create the run.
    $run_response = $client->threads()->runs()->create(
      $this->id(), [
        'assistant_id' => $this->getAssistantId(),
      ]
    );

    $this->set('current_run_id', $run_response->id);

    return $this;
  }

  /**
   * Add the message and run the prompt.
   */
  public function runPrompt(string $message): static {
    /** @var \OpenAI\Client $client */
    $client = \Drupal::service('openai.client');

    // Add the message to the thread.
    $client->threads()->messages()->create(
      $this->id(), [
        'role' => 'user',
        'content' => $message,
      ]
    );

    $this->messages[] = $message;

    // Start the run.
    $run_response = $client->threads()->runs()->create(
      $this->id(), [
        'assistant_id' => $this->getAssistantId(),
      ]
    );

    $this->set('current_run_id', $run_response->id);

    // Save the entity.
    $this->save();

    return $this;
  }

  /**
   * Store the message response.
   */
  public function storeResponse(string $message): static {
    $this->messages[] = $message;
    $this->save();
    return $this;
  }

}
