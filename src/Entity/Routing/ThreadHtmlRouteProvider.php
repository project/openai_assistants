<?php

namespace Drupal\openai_assistants\Entity\Routing;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Routing\AdminHtmlRouteProvider;
use Symfony\Component\Routing\Route;

/**
 * Provides HTML routes for thread entity.
 */
class ThreadHtmlRouteProvider extends AdminHtmlRouteProvider {

  /**
   * {@inheritdoc}
   */
  public function getRoutes(EntityTypeInterface $entity_type) {
    $collection = parent::getRoutes($entity_type);
    $entity_type_id = $entity_type->id();

    if ($thread_collection_route = $this->getAssistantThreadsRoute($entity_type)) {
      $collection->add("entity.openai_thread.threads-collection", $thread_collection_route);
    }

    return $collection;
  }

  /**
   * Gets the threads for an assistant route.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route, if available.
   */
  protected function getAssistantThreadsRoute(EntityTypeInterface $entity_type) {
    if ($entity_type->hasLinkTemplate('assistant-collection')) {
      $route = new Route($entity_type->getLinkTemplate('assistant-collection'));
      $route->setDefault('_entity_list', $entity_type->id());
      $route->setDefault('_title', 'Threads');
      $route->setRequirement('_entity_create_any_access', $entity_type->id());
      $route->setOption('parameters', ['openai_assistant' => ['type' => 'entity:openai_assistant']]);
      $route->setOption('_admin_route', TRUE);

      return $route;
    }
  }

}
