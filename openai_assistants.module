<?php

/**
 * @file
 * Provides an OpenAI assistant entity type.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\user\UserInterface;

/**
 * Prepares variables for OpenAI assistant templates.
 *
 * Default template: openai-assistant.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the OpenAI assistant
 *   information and any fields attached to the entity.
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_openai_assistant(array &$variables) {
  $variables['view_mode'] = $variables['elements']['#view_mode'];
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}

/**
 * Prepares variables for OpenAI threads templates.
 *
 * Default template: openai-thread.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the OpenAI threads information
 *   and any fields attached to the entity.
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_openai_thread(array &$variables) {
  $variables['view_mode'] = $variables['elements']['#view_mode'];
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}

/**
 * Implements hook_user_cancel().
 */
function openai_assistants_user_cancel($edit, UserInterface $account, $method) {
  switch ($method) {
    case 'user_cancel_reassign':
      // Anonymize OpenAI assistants.
      $storage = \Drupal::entityTypeManager()->getStorage('openai_assistant');
      $openai_assistant_ids = $storage->getQuery()
        ->condition('uid', $account->id())
        ->execute();
      foreach ($storage->loadMultiple($openai_assistant_ids) as $openai_assistant) {
        $openai_assistant->setOwnerId(0);
        $openai_assistant->save();
      }

      // Anonymize OpenAI threads.
      $storage = \Drupal::entityTypeManager()->getStorage('openai_thread');
      $openai_thread_ids = $storage->getQuery()
        ->condition('uid', $account->id())
        ->execute();
      foreach ($storage->loadMultiple($openai_thread_ids) as $openai_thread) {
        $openai_thread->setOwnerId(0);
        $openai_thread->save();
      }

      break;
  }
}

/**
 * Implements hook_ENTITY_TYPE_predelete() for user entities.
 */
function openai_assistants_user_predelete(UserInterface $account) {
  // Delete OpenAI assistants.
  $storage = \Drupal::entityTypeManager()->getStorage('openai_assistant');
  $openai_assistant_ids = $storage->getQuery()
    ->condition('uid', $account->id())
    ->execute();
  $openai_assistants = $storage->loadMultiple($openai_assistant_ids);
  $storage->delete($openai_assistants);
  // Delete old revisions.
  $openai_assistant_ids = $storage->getQuery()
    ->allRevisions()
    ->condition('uid', $account->id())
    ->execute();
  foreach (array_keys($openai_assistant_ids) as $revision_id) {
    $storage->deleteRevision($revision_id);
  }

  // Delete OpenAI threads.
  $storage = \Drupal::entityTypeManager()->getStorage('openai_thread');
  $openai_thread_ids = $storage->getQuery()
    ->condition('uid', $account->id())
    ->execute();
  $openai_threads = $storage->loadMultiple($openai_thread_ids);
  $storage->delete($openai_threads);
  // Delete old revisions.
  $openai_thread_ids = $storage->getQuery()
    ->allRevisions()
    ->condition('uid', $account->id())
    ->execute();
  foreach (array_keys($openai_thread_ids) as $revision_id) {
    $storage->deleteRevision($revision_id);
  }
}

/**
 * Implements hook_preprocess_block().
 */
function openai_assistants_preprocess_block(&$variables) {
  if ($variables['base_plugin_id'] == 'block_content') {
    $content = $variables['elements']['content']['#block_content'];

    if ($content->bundle() == 'openai_assistant') {
      $settings = $variables['configuration']['openai_assistants'] ?? [];
      $settings['assistant_id'] = $content?->field_assistant?->entity?->id?->value;
      $settings['title'] = $variables['label'];
      $settings['block_id'] = $variables['elements']['#id'];
      $form = \Drupal::formBuilder()
        ->getForm('Drupal\openai_assistants\Form\OpenaiChatForm', $settings);
      $variables['content']['prompt'] = $form;
      $variables['attributes']['class'][] = 'openai-assistant-block';
    }
  }
}

/**
 * Implements hook_form_alter().
 */
function openai_assistants_form_alter(array &$form, FormStateInterface $form_state) {
  $settings_provider = $form['settings']['provider']['#value'] ?? '';

  // Add settings options to block configuration form.
  if ($settings_provider == 'block_content') {
    $is_assistant = FALSE;
    $build_info = $form_state->getBuildInfo();
    $dependencies = $build_info['callback_object']?->getEntity()
      ?->getDependencies();
    $content = $dependencies['content'] ?? [];

    foreach ($content as $dependency) {
      if (strpos($dependency, 'block_content:openai_assistant:') === 0) {
        $is_assistant = TRUE;
      }
    }

    if ($is_assistant) {
      $block_id = $form['id']['#default_value'];
      $config = \Drupal::config('block.block.' . $block_id);
      $settings = $config->get('settings.openai_assistants');

      $form['openai_assistants'] = [
        '#type' => 'fieldset',
        '#title' => t('Assistant Options'),
        '#tree' => TRUE,
      ];

      $form['openai_assistants']['download'] = [
        '#type' => 'checkbox',
        '#title' => t('Download'),
        '#description' => t('Enable download button.'),
        '#default_value' => $form_state->getValue([
          'openai_assistants',
          'download',
        ], $settings['download'] ?? 0),
      ];

      $download_prompt_default = t("Provide the previous responses formatted as a page of HTML which includes <!DOCTYPE html>. The reply should only include HTML. Where appropriate use headings, paragraphs, bold, and bullet points.");

      $form['openai_assistants']['download_prompt'] = [
        '#type' => 'textarea',
        '#title' => t('Download Prompt'),
        '#description' => t('Prompt used to generate the contents of a downloadable file.'),
        '#default_value' => $form_state->getValue([
          'openai_assistants',
          'download_prompt',
        ], $settings['download_prompt'] ?? $download_prompt_default),
        '#states' => [
          'invisible' => [
            ':input[name="openai_assistants[download]"]' => ['checked' => FALSE],
          ],
        ],
      ];

      $form['openai_assistants']['threads'] = [
        '#type' => 'checkbox',
        '#title' => t('Muliple Chats'),
        '#description' => t('User can have multiple chats with an assistant and switch between them.'),
        '#default_value' => $form_state->getValue([
          'openai_assistants',
          'threads',
        ], $settings['threads'] ?? 0),
      ];

      $form['actions']['submit']['#submit'][] = 'openai_assistants_single_assistant_block_submit';
    }
  }
}

/**
 * Submit callback for single assistant block configuration.
 */
function openai_assistants_single_assistant_block_submit(&$form, FormStateInterface $form_state) {
  $block_id = $form_state->getValue('id');
  \Drupal::service('config.factory')
    ->getEditable('block.block.' . $block_id)
    ->set('settings.openai_assistants.download', $form_state->getValue([
      'openai_assistants',
      'download',
    ]))
    ->set('settings.openai_assistants.download_prompt', $form_state->getValue([
      'openai_assistants',
      'download_prompt',
    ]))
    ->set('settings.openai_assistants.threads', $form_state->getValue([
      'openai_assistants',
      'threads',
    ]))
    ->save();
}

/**
 * Implements hook_page_attachments_alter().
 */
function openai_assistants_page_attachments_alter(array &$attachments) {
  $route_match = \Drupal::routeMatch();

  if ($route_match->getRouteName() == 'entity.openai_assistant.canonical') {
    // Add the print css.
    $attachments['#attached']['library'][] = 'openai_assistants/openai-assistants-print';
  }
}
