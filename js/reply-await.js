(function ($, Drupal) {
  'use strict';

  Drupal.behaviors.openaiAssistantsReplyAwait = {
    attach: function (context, settings) {

      async function typeWriterEffect($prompt, content) {
        for (let index = 0; index < content.length; index++) {
            $prompt.append(content.charAt(index));
            await new Promise(resolve => setTimeout(resolve, 50));
        }
      }

      async function clearText($prompt) {
          $prompt.empty();
      }

      async function clearClass($prompt, $class) {
          $prompt.removeClass($class);
      }

      async function hideReply($reply_role, $reply_message) {
          $reply_role.hide();
          $reply_message.hide();
      }

      async function showReply($reply_role, $reply_message) {
          $reply_role.fadeIn();
          $reply_message.fadeIn();
          $reply_role.removeClass('role-reply-await');
      }

      async function displayLoadingDots($reply_message) {
        const dots = ['.', '..', '...'];
        let currentDotIndex = 0;

        async function animate() {
            $reply_message.text(Drupal.t('Thinking') + dots[currentDotIndex]);
            currentDotIndex = (currentDotIndex + 1) % dots.length;
            await new Promise(resolve => setTimeout(resolve, 500));
            await animate();
        }

        await animate();
    }

      async function execute() {
          const $prompt = $(context).find('.prompt-typewriter-effect');
          const content = $prompt.text();
          const $reply_role = $(context).find('.role-reply-await');
          const $reply_message = $(context).find('.message-reply-await');
          const $thinking = $(context).find('.message-reply-await .waiting');

          await hideReply($reply_role, $reply_message);
          await clearText($prompt);
          await typeWriterEffect($prompt, content);
          await clearClass($prompt, 'prompt-typewriter-effect');
          await showReply($reply_role, $reply_message);
          await displayLoadingDots($thinking);
      }

      execute();
    }
  };


})(jQuery, Drupal);