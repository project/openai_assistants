(function ($, Drupal) {
  'use strict';

  /**
   * Scroll container element to the top of the target child element.
   */
  Drupal.behaviors.openaiAssistantsScroll = {
    attach: function (context, settings) {

      const target = '.openai-assistant-scroll';
      const container = $(target).parent();
      const speed = settings.speed || 500;
      const easing = settings.easing || 'swing';

      if ($(target).length) {
        var offset = $(container).scrollTop() + $(target).position().top;

        $(container).animate({ scrollTop: offset }, speed, easing, function() {
          $(target).removeClass('openai-assistant-scroll');
        });
      }
    }
  };

})(jQuery, Drupal);