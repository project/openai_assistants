(function ($, Drupal) {
  'use strict';

  /**
   * Display text letter by letter.
   */
  Drupal.behaviors.openaiAssistantsTypewritter = {
    attach: function (context, settings) {

      async function typeWriterEffect($content, $display) {

        let typewriter = new Typewriter($display, {
          autoStart: true,
          delay: 20,
          loop: false,
          skipAddStyles: true,
        });

        typewriter
        .typeString($content)
        .start();
      }

      async function removeElement($target) {
        $(context).find($target).remove();
      }

      async function removeParent($target) {
        const $contents = $(context).find($target).contents();
        $(context).find($target).replaceWith($contents);
      }

      async function clearText($target) {
        $target.empty();
      }

      async function clearClass($target, $class) {
        $(context).find($target).removeClass($class);
      }

      async function execute() {
        const $source = '#openai-assistant-reply-content';
        const $content = $(context).find($source).html();
        const $display = '#openai-assistant-reply-display';

        // Display the reply using the typewriter effect.
        typeWriterEffect($content, $display);

        // Remove the hidden reply source.
        await removeElement($source);

        // Remove the div around the reply.
        await removeParent($display);
      }

      execute();

    }
  };

})(jQuery, Drupal);