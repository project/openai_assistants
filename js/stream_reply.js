(function ($, Drupal) {
  'use strict';

  /**
   * Display text letter by letter.
   */
  Drupal.behaviors.openaiAssistantsStreaming = {
    attach: function (context, settings) {
      const trigger = 'openai-assistants-reply-trigger';
      const $button = $('.' + trigger);
      const target = 'message-reply-await';
      const $promptField = $('#openai-assistants-prompt-field');
      const $sendButton = $('.openai-assistants-prompt-send');
      const path = '/openai_assistants/stream_reply';

      const handleClick = async () => {
        await formDisable();

        try {
          // Stream reply.
          await streamInto('.' + target, path);
        } catch (error) {
          console.error('Error fetching and streaming HTML:', error);
        } finally {
          // Set message as completed.
          await removeClassAsync('.' + target, target);
          await formEnable();
        }
      };

      // Ensure the click handler is attached only once
      if (!$button.data('openai-assistants-clicked')) {
        $button.on('mousedown', handleClick);
        $button.data('openai-assistants-clicked', true);
      }

      async function formDisable() {
        $button.prop('disabled', true);
        $promptField.prop('disabled', true);
        $sendButton.prop('disabled', true);
      }

      async function formEnable() {
        // Enable the prompt form field.
        $promptField.prop('disabled', false);
        // Enable the prompt send button.
        $sendButton.prop('disabled', false);
        // Enable the hidden trigger button.
        $button.prop('disabled', false);
      }

      async function streamInto(targetElement, $endpoint) {
        const response = await fetch($endpoint);

        if (!response.ok) {
          throw new Error(`HTTP error! Status: ${response.status}`);
        }

        await response.body
          .pipeThrough(new TextDecoderStream())
          .pipeTo(appendToDOM(targetElement));
      }

      function appendToDOM(targetElement) {
        $(targetElement).empty();

        const newDocument = document.implementation.createHTMLDocument();
        newDocument.write("<div>");
        $(targetElement).append(newDocument.body.firstChild);

        return new WritableStream({
          write(chunk) {
            newDocument.write(chunk);
          },
          close() {
            newDocument.close();
          },
          abort(reason) {
            newDocument.close();
          }
        });
      }

      async function removeClassAsync(selector, className) {
        $(selector).removeClass(className);
      }

    }
  };

})(jQuery, Drupal);
