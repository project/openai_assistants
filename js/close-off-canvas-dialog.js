(function ($, Drupal) {
  'use strict';

  /**
   * Display initial prompt block on dialog close.
   */
  Drupal.behaviors.openaiAssistantsDialogClose = {
    attach: function (context, settings) {
      $('.openai-assistant-dialog-offcanvas').on('dialogclose', function(event) {
        $('.openai-assistant-block').css('display', 'block');
      });
    }
  };

})(jQuery, Drupal);