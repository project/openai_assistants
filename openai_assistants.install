<?php

/**
 * @file
 * Install, update and uninstall functions for the OpenAi Assistants module.
 */

use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Implements hook_uninstall().
 */
function openai_assistants_uninstall($is_syncing) {
  $block_type = \Drupal::entityTypeManager()
    ->getStorage('block_content_type')
    ->load('openai_assistant');

  if (empty($block_type)) {
    return;
  }

  // Get all possible blocks with block type = "openai_assistant".
  $blocks = \Drupal::entityTypeManager()
    ->getStorage('block_content')
    ->loadByProperties(['type' => 'openai_assistant']);

  // Delete blocks of type "openai_assistant".
  if (!empty($blocks)) {
    foreach ($blocks as $block) {
      $block->delete();
    }
  }

  // Delete the block type.
  $block_type->delete();

  // Delete OpenAI Assistant configuration.
  $module_path = \Drupal::service('extension.list.module')
    ->getPath('openai_assistants') . '/config/install';

  $configs = [];
  $files = \Drupal::service('file_system')
    ->scanDirectory($module_path, '/\.yml$/');

  if ($files) {
    foreach ($files as $file) {
      $configs[] = $file->name;
    }

    foreach ($configs as $config_name) {
      \Drupal::configFactory()->getEditable($config_name)->delete();
    }

    return TRUE;
  }
}

/**
 * Add file upload field.
 */
function openai_assistants_update_101003() {
  $field_storage_definition = BaseFieldDefinition::create('entity_reference')
    ->setLabel(t('Files'))
    ->setDescription(t('Add documents for the AI Assistant to base answers on.'))
    ->setSetting('target_type', 'media')
    ->setCardinality(20)
    ->setSetting('handler', 'default:media')
    ->setSetting('handler_settings', [
      'target_bundles' => ['ai_data'],
      'auto_create' => TRUE,
      'auto_create_bundle' => '',

    ])
    ->setDisplayOptions('form', [
      'type' => 'media_library_widget',
      'weight' => 0,
      'settings' => [
        'media_types' => ['ai_data'],
      ],
    ])
    ->setDisplayConfigurable('form', TRUE);

  \Drupal::entityDefinitionUpdateManager()
    ->installFieldStorageDefinition('file_ids', 'openai_assistant', 'openai_assistants', $field_storage_definition);
}
